package com.example.josu.jobboard.Activities;

import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.josu.jobboard.Fragments.FragmentCurriculum;
import com.example.josu.jobboard.Fragments.FragmentList;
import com.example.josu.jobboard.Fragments.FragmentMessages;
import com.example.josu.jobboard.Fragments.FragmentSearch;
import com.example.josu.jobboard.R;

import java.util.ArrayList;
import java.util.List;

public class MainUI extends AppCompatActivity {
    private TabLayout tabLayout;
    private ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_ui);
        tabLayout = findViewById(R.id.tabs);
        viewPager = findViewById(R.id.pagerTarget);
        tabLayout.setupWithViewPager(viewPager);
        setupViewPager(viewPager);
        setupIcons();
    }

    private void setupViewPager(ViewPager viewPager){
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new FragmentList(), "Lista");
        adapter.addFragment(new FragmentSearch(), "Busqueda");
        adapter.addFragment(new FragmentMessages(), "Mensajes");
        adapter.addFragment(new FragmentCurriculum(), "Perfil");
        viewPager.setAdapter(adapter);
    }

    private void  setupIcons(){
        tabLayout.getTabAt(0).setIcon(R.drawable.ic_home_black_48px);
        tabLayout.getTabAt(1).setIcon(R.drawable.ic_search_black_48px);
        tabLayout.getTabAt(2).setIcon(R.drawable.ic_mail_outline_black_48px);
        tabLayout.getTabAt(3).setIcon(R.drawable.ic_description_black_48px);
    }



    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> fragmentList = new ArrayList<>();
        private final List<String> titleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        public void addFragment(Fragment fragment, String title){
            fragmentList.add(fragment);
            titleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return titleList.get(position);
        }

        @Override
        public Fragment getItem(int position) {
            return fragmentList.get(position);
        }

        @Override
        public int getCount() {
            return fragmentList.size();
        }
    }
}

