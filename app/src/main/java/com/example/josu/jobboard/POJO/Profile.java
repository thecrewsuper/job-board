package com.example.josu.jobboard.POJO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Josué on 20/05/2018.
 */

public class Profile {
    @SerializedName("id")
    @Expose
    private String id;

    public Profile(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
