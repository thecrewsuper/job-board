package com.example.josu.jobboard.Activities;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.josu.jobboard.API.Client;
import com.example.josu.jobboard.Activities.Adapters.RegisterFormAdapter;
import com.example.josu.jobboard.Fragments.Adapters.PerfilAdapter;
import com.example.josu.jobboard.POJO.Achievement;
import com.example.josu.jobboard.POJO.Education;
import com.example.josu.jobboard.POJO.Student;
import com.example.josu.jobboard.POJO.Student_;
import com.example.josu.jobboard.POJO.Token;
import com.example.josu.jobboard.R;
import com.example.josu.jobboard.Utils.Constants;
import com.example.josu.jobboard.Utils.TemporalData;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import rx.Observer;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class RegisterFormActivity extends AppCompatActivity {

  Spinner spinner;
  private RecyclerView rvAptitud, rvLogros;
  private RecyclerView.Adapter rvAdapterApti, rvAdapterLogr;
  private RecyclerView.LayoutManager rvLMApti, rvLMLogr;
  EditText etSkills, etAchievName, etAchievDescrip, etFecha, etIngreso, etEgreso;
  ImageButton btnFechaIngr, btnFechaEgr, btnAddSkill, btnAddAchiev, btnFechaAchiev;
  private RadioButton rbMale, rbFemale;
  private Button btnFinish;
  private List<String> lstSkills = new ArrayList<>();
  private List<Achievement> lstAchievements = new ArrayList<>();
  private Subscription subscription;
  private Token globalToken;
  private ProgressDialog dialog;

  private static final String CERO = "0";
  private static final String BARRA = "/";

  //Calendario para obtener fecha & hora
  public final Calendar c = Calendar.getInstance();

  //Variables para obtener la fecha
  final int mes = c.get(Calendar.MONTH);
  final int dia = c.get(Calendar.DAY_OF_MONTH);
  final int anio = c.get(Calendar.YEAR);

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_register_form);
    SpinnerOptions();
    etSkills = findViewById(R.id.etSkill);
    etAchievName = findViewById(R.id.etAchievName);
    etAchievDescrip = findViewById(R.id.etAchievDescrip);
    etFecha = findViewById(R.id.etFecha);
    etIngreso = findViewById(R.id.etIngreso);
    etEgreso = findViewById(R.id.etEgreso);
    rbMale = findViewById(R.id.rbMale);
    rbFemale = findViewById(R.id.rbFemale);
    btnFechaIngr = findViewById(R.id.ibBtnIngreso);
    btnFechaEgr = findViewById(R.id.ibBtnEgreso);
    btnAddSkill = findViewById(R.id.ibBtnAddSkill);
    btnAddAchiev = findViewById(R.id.ibBtnAddAchie);
    btnFechaAchiev = findViewById(R.id.ibBtnFechaAchie);
    btnFinish = findViewById(R.id.btnFinish);
    dialog = new ProgressDialog(this);

    btnFechaIngr.setOnClickListener(view -> {
      //0 PARA INGRESO
      obtenerFecha(0);
    });

    btnFechaEgr.setOnClickListener(view -> {
      //1 PARA INGRESO
      obtenerFecha(1);
    });

    btnFechaAchiev.setOnClickListener(view -> {
      //1 PARA INGRESO
      obtenerFecha(3);
    });

    etIngreso.setOnClickListener(v -> obtenerFecha(0));

    etEgreso.setOnClickListener(v -> obtenerFecha(1));

    etFecha.setOnClickListener(v -> obtenerFecha(3));

    btnAddSkill.setOnClickListener(view -> addSkill());

    btnAddAchiev.setOnClickListener(view -> addAchievment());

    btnFinish.setOnClickListener(v -> onClickFinish());

    getUserAccessToken();
    Listas();
  }

  public void Listas() {
    //OBTENER RECYCLER
    //APTITUDES
    rvAptitud = findViewById(R.id.rvListSkills);
    rvAptitud.setHasFixedSize(true);
    //LOGROS
    rvLogros = findViewById(R.id.rvListAchie);
    rvLogros.setHasFixedSize(true);

    //YSAR UN ADMIN PARA LAYOUT
    //APTITUDES
    rvLMApti = new GridLayoutManager(this, 4);
    rvAptitud.setLayoutManager(rvLMApti);
    //LOGROS
    rvLMLogr = new LinearLayoutManager(this);
    rvLogros.setLayoutManager(rvLMLogr);

    //CREAR EL ADAPTER
    //APTITUDES
    rvAdapterApti = new RegisterFormAdapter(lstSkills, null, true, this);
    rvAptitud.setAdapter(rvAdapterApti);
    //LOGROS
    rvAdapterLogr = new RegisterFormAdapter(null, lstAchievements, false, this);
    rvLogros.setAdapter(rvAdapterLogr);
  }

  public void SpinnerOptions() {
    spinner = findViewById(R.id.sSpinner);
    String[] degree = {"Ingeniería en Informática", "Ingeniería en Diseño Industrial",
        "Ingeniería en Gestión Empresarial", "Ingeniería en Industrial",
        "Ingeniería en Sistemas Computacionales", "Arquitectura", "Licenciatura en administración"};
    spinner.setAdapter(new ArrayAdapter<String>(this, R.layout.spinner_degree, degree));
  }

  private void obtenerFecha(int iMode) {
    //Estos valores deben ir en ese orden, de lo contrario no mostrara la fecha actual
/**
 *También puede cargar los valores que usted desee
 */
    DatePickerDialog recogerFecha = new DatePickerDialog(this, (view, year, month, dayOfMonth) -> {
      //Esta variable lo que realiza es aumentar en uno el mes ya que comienza desde 0 = enero
      final int mesActual = month + 1;
      //Formateo el día obtenido: antepone el 0 si son menores de 10
      String diaFormateado =
          (dayOfMonth < 10) ? CERO + String.valueOf(dayOfMonth) : String.valueOf(dayOfMonth);
      //Formateo el mes obtenido: antepone el 0 si son menores de 10
      String mesFormateado =
          (mesActual < 10) ? CERO + String.valueOf(mesActual) : String.valueOf(mesActual);
      //Muestro la fecha con el formato deseado
      if (iMode == 0) {
        etIngreso.setText(diaFormateado + BARRA + mesFormateado + BARRA + year);
      } else if (iMode == 1) {
        etEgreso.setText(diaFormateado + BARRA + mesFormateado + BARRA + year);
      } else if (iMode == 3) {
        etFecha.setText(diaFormateado + BARRA + mesFormateado + BARRA + year);
      }

    }, anio, mes, dia);
    //Muestro el widget
    recogerFecha.show();

  }

  private void onClickFinish() {
    //Queda pendiente evaluar el formulario completo
    updateStudent();
  }

  private void startLoading() {
    dialog.setMessage("Espere un momento");
    dialog.setTitle("Obteniendo Informacion");
    dialog.setIndeterminate(false);
    dialog.setCancelable(false);
    dialog.show();
  }

  private void startRequestMessage() {
    dialog.setMessage("Espere un momento");
    dialog.setTitle("Enviando Informacion");
    dialog.setIndeterminate(false);
    dialog.setCancelable(false);
    dialog.show();
  }

  private void addSkill() {
    if (!etSkills.getText().toString().isEmpty()) {
      lstSkills.add(etSkills.getText().toString());
      rvAdapterApti.notifyDataSetChanged();
      etSkills.setText("");
    }
  }

  private Student_ buildStudentInfo() {
    String genre;
    if (rbFemale.isChecked()) {
      genre = "mujer";
    } else {
      genre = "hombre";
    }
    return new Student_(globalToken.getToken(),
        genre,
        new Education(spinner.getSelectedItem().toString(),
            etIngreso.getText().toString() + " - "
                + etEgreso.getText().toString()),
        lstSkills,
        lstAchievements);
  }

  private void addAchievment() {
    if (!etAchievName.getText().toString().isEmpty()
        && !etFecha.getText().toString().isEmpty()
        && !etAchievDescrip.getText().toString().isEmpty()) {
      lstAchievements.add(new Achievement(
          etAchievName.getText().toString(),
          etAchievDescrip.getText().toString(),
          etFecha.getText().toString()
      ));
      rvAdapterLogr.notifyDataSetChanged();
      etAchievName.setText("");
      etFecha.setText("");
      etAchievDescrip.setText("");
    }
  }

  private void getUserAccessToken() {
    startLoading();
    String username = TemporalData.temporalUser.getEmail();
    String password = TemporalData.temporalUser.getPassword();

    String credentials = encodeCredential(username, password);

    subscription = Client.getClient()
        .getToken(credentials, Constants.MASTER_TOKEN)
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(new Observer<Token>() {
          @Override
          public void onCompleted() {
            dialog.dismiss();
          }

          @Override
          public void onError(Throwable e) {
            e.printStackTrace();
            getUserAccessToken();
          }

          @Override
          public void onNext(Token token) {
            if (!token.getToken().isEmpty()) { // si existe el token
              globalToken = token;
            }
          }
        });

  }

  private void updateStudent() {
    startRequestMessage();
    subscription = Client.getClient()
        .updateStudent(buildStudentInfo(), globalToken.getUser().getProfile().getId())
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(new Observer<Student>() {
          @Override
          public void onCompleted() {
            dialog.dismiss();
            launchPostRegister();
          }

          @Override
          public void onError(Throwable e) {
            dialog.dismiss();
            Toast.makeText(RegisterFormActivity.this, "Algo salio mal", Toast.LENGTH_SHORT).show();
          }

          @Override
          public void onNext(Student student) {

          }
        });

  }

  private void launchPostRegister() {
    startActivity(new Intent(this, PostRegister.class));
    finish();
  }

  private String encodeCredential(String username, String password) {
    String encodedCredentials = "Basic " + Base64
        .encodeToString(String.format("%s:%s", username, password).getBytes(), Base64.NO_WRAP);
    return encodedCredentials;
  }

}
