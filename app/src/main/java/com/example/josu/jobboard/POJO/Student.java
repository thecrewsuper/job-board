package com.example.josu.jobboard.POJO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Josué on 20/05/2018.
 */

public class Student {
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("genre")
    @Expose
    private String genre;
    @SerializedName("education")
    @Expose
    private Education education;
    @SerializedName("skills")
    @Expose
    private List<String> skills = null;
    @SerializedName("achievements")
    @Expose
    private List<Achievement> achievements = null;
    @SerializedName("user")
    @Expose
    private User user;
    @SerializedName("createdAt")
    @Expose
    private String createdAt;
    @SerializedName("updatedAt")
    @Expose
    private String updatedAt;
    @SerializedName("favorites")
    @Expose
    private List<Opening> favorites = null;

    public Student(String id, String genre, Education education, List<String> skills, List<Achievement> achievements, User user, String createdAt, String updatedAt, List<Opening> favorites) {
        this.id = id;
        this.genre = genre;
        this.education = education;
        this.skills = skills;
        this.achievements = achievements;
        this.user = user;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
        this.favorites = favorites;
    }

    public List<Opening> getFavorites() {
        return favorites;
    }

    public void setFavorites(List<Opening> favorites) {
        this.favorites = favorites;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public Education getEducation() {
        return education;
    }

    public void setEducation(Education education) {
        this.education = education;
    }

    public List<String> getSkills() {
        return skills;
    }

    public void setSkills(List<String> skills) {
        this.skills = skills;
    }

    public List<Achievement> getAchievements() {
        return achievements;
    }

    public void setAchievements(List<Achievement> achievements) {
        this.achievements = achievements;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }
}
