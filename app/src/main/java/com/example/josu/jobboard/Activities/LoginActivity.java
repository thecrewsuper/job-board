package com.example.josu.jobboard.Activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.view.View;
import android.widget.Button;

import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.example.josu.jobboard.API.Client;
import com.example.josu.jobboard.POJO.Student;
import com.example.josu.jobboard.POJO.Token;
import com.example.josu.jobboard.POJO.User;
import com.example.josu.jobboard.R;
import com.example.josu.jobboard.Utils.Constants;
import com.example.josu.jobboard.Utils.Preferences;
import com.example.josu.jobboard.Utils.TemporalData;

import rx.Observer;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;


public class LoginActivity extends AppCompatActivity {
  private Subscription subscription;
  private Button btnSignIn, btnForgot, btnSignUp;
  private EditText txtUsername, txtPassword;
  private Preferences preferences;
  private CheckBox chBRememberMe;
  private boolean flagLogin = false;
  private TextView txtVwMessage;
  private ProgressDialog dialog;
  private User globalUser;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_login);
    btnSignIn = findViewById(R.id.btnSignIn);
    txtUsername = findViewById(R.id.txt_username);
    txtPassword = findViewById(R.id.txt_password);
    btnForgot = findViewById(R.id.btnForgot);
    btnSignUp = findViewById(R.id.btnSignUp);
    txtVwMessage = findViewById(R.id.txtVwMessage);
    chBRememberMe = findViewById(R.id.chBRememberMe);

    dialog = new ProgressDialog(this);
    preferences = new Preferences(this);
    flagLogin = preferences.getShared().getBoolean(Constants.LOGIN_FLAG,false);
    if(flagLogin){
      hideButtons();
      startLoading();
      getCurrentUserInfo();
    }else{
      txtVwMessage.setVisibility(View.INVISIBLE);
    }

    btnSignIn.setOnClickListener(v -> login());
    btnSignUp.setOnClickListener(v -> onClickSignUp());
    btnForgot.setOnClickListener(view -> {
        Intent inMain = new Intent(this,RegisterFormActivity.class);
        startActivity(inMain);
    });
  }

  private void onClickSignUp(){
      startActivity(new Intent(this, RegisterActivity.class));
  }


  private void startLoading(){
      dialog.setMessage("Cargando...");
      dialog.setTitle("Iniciando Sesion");
      dialog.setIndeterminate(false);
      dialog.setCancelable(false);
      dialog.show();
  }

  private void login() {
      startLoading();
    String username = txtUsername.getText().toString();
    String password = txtPassword.getText().toString();

    String credentials = encodeCredential(username, password);

    subscription = Client.getClient()
        .getToken(credentials, Constants.MASTER_TOKEN)
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(new Observer<Token>() {
          @Override
          public void onCompleted() {
             getStudent();
          }

          @Override
          public void onError(Throwable e) {
            e.printStackTrace();
            // si es un 401 es un error de autentificacion
            if (e.getMessage().contains("401")) {
                dialog.dismiss();
              Toast.makeText(LoginActivity.this, "Usuario o contraseña incorrecta",
                  Toast.LENGTH_SHORT).show();
            }
          }

          @Override
          public void onNext(Token token) {
            if (!token.getToken().isEmpty()) { // si existe el token
             saveData(token);
                globalUser = token.getUser();
                TemporalData.userID = token.getUser().getProfile().getId();
            }else{
              Toast.makeText(LoginActivity.this, "Fallo inicio de sesion", Toast.LENGTH_SHORT).show();
            }
          }
        });

  }

  private void hideButtons(){
    btnSignUp.setVisibility(View.INVISIBLE);
    btnSignIn.setVisibility(View.INVISIBLE);
    btnForgot.setVisibility(View.INVISIBLE);
  }

  private void getCurrentUserInfo(){
    String userToken = preferences.getShared().getString(Constants.USER_TOKEN,"");
    subscription = Client.getClient().getCurrentUser(userToken)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(new Observer<User>() {
              @Override
              public void onCompleted() {
                getStudent();
              }

              @Override
              public void onError(Throwable e) {
                dialog.dismiss();
              }

              @Override
              public void onNext(User user) {
                  TemporalData.user = user;
                  globalUser = user;
              }
            });
  }

  private void saveData(Token token){
      if(chBRememberMe.isChecked()){
          flagLogin = true;
          preferences.getEditor().putString(Constants.USER_TOKEN, token.getToken());
          preferences.getEditor().putString(Constants.USER_NAME,token.getUser().getName());
          preferences.getEditor().putString(Constants.USER_EMAIL,token.getUser().getEmail());
          preferences.getEditor().putString(Constants.USER_ID,token.getUser().getId());
          preferences.getEditor().putBoolean(Constants.LOGIN_FLAG,flagLogin);
          TemporalData.user = token.getUser();
      }
      preferences.getEditor().putString(Constants.USER_TOKEN, token.getToken());
      TemporalData.user = token.getUser();
      preferences.getEditor().apply();
  }

  private void getStudent(){
        String Authorization = "Bearer "+preferences.getShared().getString(Constants.USER_TOKEN,"");
            subscription = Client.getClient()
              .getStudent(globalUser.getProfile().getId(),Authorization)
              .subscribeOn(Schedulers.io())
              .observeOn(AndroidSchedulers.mainThread())
              .subscribe(new Observer<Student>() {
                  @Override
                  public void onCompleted() {
                      dialog.dismiss();
                      launchUI();
                  }

                  @Override
                  public void onError(Throwable e) {
                      dialog.dismiss();
                      Toast.makeText(LoginActivity.this, "Algo salio mal, intentalo de nuevo", Toast.LENGTH_SHORT).show();

                  }

                  @Override
                  public void onNext(Student student) {
                    TemporalData.temporalStudent = student;
                   }
              });
  }

  private void launchUI() {
    startActivity(new Intent(this, MainUI.class));
    finish();
  }

  private String encodeCredential(String username, String password) {
    String encodedCredentials = "Basic " + Base64
        .encodeToString(String.format("%s:%s", username, password).getBytes(), Base64.NO_WRAP);
    return encodedCredentials;
  }
}