package com.example.josu.jobboard.API;

import com.example.josu.jobboard.POJO.FavoriteResponse;
import com.example.josu.jobboard.POJO.Opening;
import com.example.josu.jobboard.POJO.Student;
import com.example.josu.jobboard.POJO.Student_;
import com.example.josu.jobboard.POJO.Token;
import com.example.josu.jobboard.POJO.User;
import com.example.josu.jobboard.POJO.User_;
import com.example.josu.jobboard.Utils.Constants;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

import android.support.annotation.NonNull;
import rx.Observable;
/**
 * Created by Josué on 05/04/2018.
 */

public class Client {

  private static Client client;
  private static Endpoints endpoints;

  public static Client getClient() {
    if (client == null) {
      client = new Client();
    }
    return client;
  }

  private Client() {
    final Gson gson = new GsonBuilder()
        .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
        .create();

    HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
    interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

    OkHttpClient client = new OkHttpClient.Builder()
        .addInterceptor(interceptor)
        .retryOnConnectionFailure(true)
        .connectTimeout(30, TimeUnit.SECONDS)
        .readTimeout(30, TimeUnit.SECONDS)
        .build();

    final Retrofit retrofit = new Retrofit.Builder()
        .baseUrl(Constants.BASE_URL)
        .client(client)
        .addCallAdapterFactory(RxJavaCallAdapterFactory.createAsync())
        .addConverterFactory(GsonConverterFactory.create(gson))
        .build();

    endpoints = retrofit.create(Endpoints.class);
  }

  public Observable<Token> getToken(@NonNull String credentials, @NonNull String masterToken) {
    return endpoints.getToken(credentials, masterToken);
  }

  public Observable<User> getCurrentUser(String userToken){
    return endpoints.getCurrentUser(userToken);
  }

  public Observable<Student> updateStudent(Student_ student, String ProfileId){
    return endpoints.updateStudent(student, ProfileId);
  }

  public Observable<User> createUser(User_ user){
    return endpoints.createUser(user);
  }

  public Observable<Student> getStudent(String profileId, String userToken){
    return endpoints.getStudent(profileId, userToken);
  }

  public Observable<List<Opening>> getOpenings(String carrera){
    return endpoints.getOpenings(carrera);
  }
  public Observable<Opening> getOpening(String id){
    return endpoints.getOpening(id);
  }

  public Observable<FavoriteResponse> updateUserFavorite(Student_ student, String ProfileId){
    return endpoints.updateStudentFavorite(student, ProfileId);
  }



}
