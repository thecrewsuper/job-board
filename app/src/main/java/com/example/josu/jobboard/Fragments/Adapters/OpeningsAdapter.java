package com.example.josu.jobboard.Fragments.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.josu.jobboard.POJO.Opening;
import com.example.josu.jobboard.R;
import com.example.josu.jobboard.Utils.OnGeneralClickListener;


import java.util.List;

/**
 * Created by Josué on 27/05/2018.
 */

public class OpeningsAdapter extends RecyclerView.Adapter<OpeningsAdapter.ViewHolder> {
    private final Context context;
    private final LayoutInflater inflater;
    private final List<Opening> openings;
    private final OnGeneralClickListener listener;

    public OpeningsAdapter(Context context, List<Opening> openings, OnGeneralClickListener listener) {
        this.context = context;
        this.openings = openings;
        this.listener = listener;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = inflater.inflate(R.layout.layout_ofertas,parent,false);
        return new ViewHolder(itemView, listener);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Opening opening = openings.get(position);
        holder.tvPuesto.setText(opening.getTitle());
        holder.tvSalario.setText("$ "+opening.getSalary());
        holder.tvLugar.setText(opening.getLocation().getCity()+", "+opening.getLocation().getState());
        holder.tvNombreEmpresaOF.setText(opening.getCompany().getName());

    }

    @Override
    public int getItemCount() {
        return openings.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        private TextView tvSalario, tvPuesto, tvLugar, tvNombreEmpresaOF;
        private OnGeneralClickListener listener;

        public ViewHolder(View itemView, OnGeneralClickListener listener) {
            super(itemView);
            tvSalario = itemView.findViewById(R.id.tvSalario);
            tvPuesto = itemView.findViewById(R.id.tvPuesto);
            tvLugar = itemView.findViewById(R.id.tvLugar);
            tvNombreEmpresaOF = itemView.findViewById(R.id.tvNombreEmpresaOF);
            this.listener = listener;
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            listener.onClick(view, getAdapterPosition());
        }
    }
}
