package com.example.josu.jobboard.Fragments;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.example.josu.jobboard.Activities.MainUI;
import com.example.josu.jobboard.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentSearch extends Fragment {

    Button btnSearch;
    MainUI mainUI;

    public FragmentSearch() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mainUI = (MainUI)getActivity();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_fragment_search, container, false);

        btnSearch = v.findViewById(R.id.btnSearchS);

        btnSearch.setOnClickListener(view -> {
            Toast.makeText(mainUI, "Estamos en proceso de crear magia!", Toast.LENGTH_SHORT).show();
        });

        return v;
    }

}