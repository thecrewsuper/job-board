package com.example.josu.jobboard.POJO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Josué on 20/05/2018.
 */

public class Student_ {
    @SerializedName("access_token")
    @Expose
    private String accessToken;
    @SerializedName("genre")
    @Expose
    private String genre;
    @SerializedName("education")
    @Expose
    private Education education;
    @SerializedName("skills")
    @Expose
    private List<String> skills = null;
    @SerializedName("achievements")
    @Expose
    private List<Achievement> achievements = null;
    @SerializedName("favorites")
    @Expose
    private List<String> favorites = null;

    public Student_(String accessToken, String genre, Education education, List<String> skills, List<Achievement> achievements) {
        this.accessToken = accessToken;
        this.genre = genre;
        this.education = education;
        this.skills = skills;
        this.achievements = achievements;
    }

    public Student_(String accessToken, List<String> skills, List<Achievement> achievements) {
        this.accessToken = accessToken;
        this.skills = skills;
        this.achievements = achievements;
    }

    public Student_(String accessToken, List<String> favorites, List<String> skills, List<Achievement> achievements) {
        this.accessToken = accessToken;
        this.favorites = favorites;
        this.skills = skills;
        this.achievements = achievements;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public Education getEducation() {
        return education;
    }

    public void setEducation(Education education) {
        this.education = education;
    }

    public List<String> getSkills() {
        return skills;
    }

    public void setSkills(List<String> skills) {
        this.skills = skills;
    }

    public List<Achievement> getAchievements() {
        return achievements;
    }

    public void setAchievements(List<Achievement> achievements) {
        this.achievements = achievements;
    }
}
