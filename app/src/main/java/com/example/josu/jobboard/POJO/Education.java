package com.example.josu.jobboard.POJO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Josué on 20/05/2018.
 */

public class Education {
    @SerializedName("degree")
    @Expose
    private String degree;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("grade")
    @Expose
    private String grade;

    public Education(String degree, String date, String grade) {
        this.degree = degree;
        this.date = date;
        this.grade = grade;
    }

    public Education(String degree, String date) {
        this.degree = degree;
        this.date = date;
    }

    public String getDegree() {
        return degree;
    }

    public void setDegree(String degree) {
        this.degree = degree;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }
}
