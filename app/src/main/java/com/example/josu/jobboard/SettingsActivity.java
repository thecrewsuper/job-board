package com.example.josu.jobboard;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;

import com.example.josu.jobboard.Activities.LoginActivity;
import com.example.josu.jobboard.Activities.MainUI;
import com.example.josu.jobboard.Utils.Preferences;

public class SettingsActivity extends AppCompatActivity {
    private Preferences preferences;
    private Button btnLogOut;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        preferences = new Preferences(this);
        btnLogOut = findViewById(R.id.btnLogOut);
        btnLogOut.setOnClickListener(v -> logOut());
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(this, MainUI.class));
        finish();
    }

    private void logOut(){
        preferences.getEditor().clear();
        preferences.getEditor().apply();
        startActivity(new Intent(this, LoginActivity.class));
        finish();
    }
}
