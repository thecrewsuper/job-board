package com.example.josu.jobboard.Utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Josué on 01/05/2018.
 */

public class Preferences {
    private  SharedPreferences shared;
    private  SharedPreferences.Editor editor;

    public Preferences(Context context){
        shared = context.getSharedPreferences(Constants.APP_NAME, Context.MODE_PRIVATE);
        editor = shared.edit();
    }

    public SharedPreferences getShared() {
        return shared;
    }

    public SharedPreferences.Editor getEditor() {
        return editor;
    }
}
