package com.example.josu.jobboard.POJO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Josué on 27/05/2018.
 */

public class Opening {
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("location")
    @Expose
    private Location location;
    @SerializedName("salary")
    @Expose
    private String salary;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("carreer")
    @Expose
    private List<String> carreer = null;
    @SerializedName("company")
    @Expose
    private Company_ company;

    public Opening(String id, String title, Location location, String salary, String date, String description, List<String> carreer, Company_ company) {
        this.id = id;
        this.title = title;
        this.location = location;
        this.salary = salary;
        this.date = date;
        this.description = description;
        this.carreer = carreer;
        this.company = company;
    }

    public String getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public Location getLocation() {
        return location;
    }

    public String getSalary() {
        return salary;
    }

    public String getDate() {
        return date;
    }

    public String getDescription() {
        return description;
    }

    public List<String> getCarreer() {
        return carreer;
    }

    public Company_ getCompany() {
        return company;
    }
}
