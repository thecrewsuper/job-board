package com.example.josu.jobboard.POJO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Josué on 27/05/2018.
 */

public class Company_ {
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("user")
    @Expose
    private User user;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("rfc")
    @Expose
    private String rfc;
    @SerializedName("razon")
    @Expose
    private String razon;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("openings")
    @Expose
    private List<Opening> openings = null;

    public Company_(String id, User user, String name, String rfc, String razon, String description, List<Opening> openings) {
        this.id = id;
        this.user = user;
        this.name = name;
        this.rfc = rfc;
        this.razon = razon;
        this.description = description;
        this.openings = openings;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<Opening> getOpenings() {
        return openings;
    }

    public void setOpenings(List<Opening> openings) {
        this.openings = openings;
    }

    public String getName() {
        return name;
    }

    public String getRfc() {
        return rfc;
    }

    public String getRazon() {
        return razon;
    }

    public String getDescription() {
        return description;
    }
}
