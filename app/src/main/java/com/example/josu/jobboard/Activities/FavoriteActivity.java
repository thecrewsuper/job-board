package com.example.josu.jobboard.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.example.josu.jobboard.Fragments.Adapters.OpeningsAdapter;
import com.example.josu.jobboard.POJO.Opening;
import com.example.josu.jobboard.R;
import com.example.josu.jobboard.Utils.Constants;
import com.example.josu.jobboard.Utils.OnGeneralClickListener;
import com.example.josu.jobboard.Utils.TemporalData;

import java.util.List;

public class FavoriteActivity extends AppCompatActivity {
    private LinearLayoutManager linearLayoutManager;
    private RecyclerView rcVwFavoritos;
    private OnGeneralClickListener listener;
    private List<Opening> favorites = TemporalData.temporalStudent.getFavorites();
    private OpeningsAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favorite);
        rcVwFavoritos = findViewById(R.id.rcVwFavoritos);
        linearLayoutManager = new LinearLayoutManager(this);
        listener = (v, pos) -> onClickFavorite(pos);
        adapter = new OpeningsAdapter(this,favorites,listener);
        rcVwFavoritos.setLayoutManager(linearLayoutManager);
        rcVwFavoritos.setAdapter(adapter);

    }

    private void onClickFavorite(int pos){
        Opening opening = favorites.get(pos);
        Intent intent = new Intent(this, OpeningActivity.class);
        intent.putExtra(Constants.OPENING_ID, opening.getId());
        intent.putExtra(Constants.FAVORITE,Constants.IS_FAVORITE);
        startActivity(intent);
    }
}
