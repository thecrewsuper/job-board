package com.example.josu.jobboard.Activities;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.example.josu.jobboard.API.Client;
import com.example.josu.jobboard.Activities.Adapters.AchievmentsAdapter;
import com.example.josu.jobboard.Activities.Adapters.SkillsAdapter;
import com.example.josu.jobboard.POJO.Achievement;
import com.example.josu.jobboard.POJO.Student;
import com.example.josu.jobboard.POJO.Student_;
import com.example.josu.jobboard.R;
import com.example.josu.jobboard.Utils.Constants;
import com.example.josu.jobboard.Utils.OnGeneralClickListener;
import com.example.josu.jobboard.Utils.Preferences;
import com.example.josu.jobboard.Utils.TemporalData;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import rx.Observer;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class EditUserActivity extends AppCompatActivity {
    private EditText edtTxtSkills, edtTxtLogroName, edtTxtDateLogro,edtTxtDescriptionLogro;
    private ImageButton btnSkillsAdd, btnDateLogro, btnLogroAdd;
    private RecyclerView rcVwSkills, rcVwLogro;
    private Button btnEditFinalizar;
    private List<String> temporalSkills;
    private Preferences preferences;
    private List<Achievement> temporalAchievments;
    private RecyclerView.LayoutManager layoutManagerSkills, layoutManagerAchievments;
    private AchievmentsAdapter achivmientsAdapter;
    private SkillsAdapter skillsAdapter;
    private OnGeneralClickListener listenerAchievments, listenerSkills;
    private Subscription subscription;
    private ProgressDialog dialog;
    private Student globalStudent;

    public final Calendar c = Calendar.getInstance();
    final int mes = c.get(Calendar.MONTH);
    final int dia = c.get(Calendar.DAY_OF_MONTH);
    final int anio = c.get(Calendar.YEAR);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_user);
        edtTxtSkills = findViewById(R.id.edtTxtSkills);
        edtTxtLogroName = findViewById(R.id.edtTxtLogroName);
        edtTxtDateLogro = findViewById(R.id.edtTxtDateLogro);
        edtTxtDescriptionLogro = findViewById(R.id.edtTxtDescriptionLogro);
        btnSkillsAdd = findViewById(R.id.btnSkillsAdd);
        btnDateLogro = findViewById(R.id.btnDateLogro);
        btnLogroAdd = findViewById(R.id.btnLogroAdd);
        rcVwSkills = findViewById(R.id.rcVwSkills);
        rcVwLogro = findViewById(R.id.rcVwLogro);
        btnEditFinalizar = findViewById(R.id.btnEditFinalizar);
        dialog = new ProgressDialog(this);
        preferences = new Preferences(this);

        listenerAchievments = (v, pos) -> onClickDeleteAchievment(pos);
        listenerSkills = (v, pos) -> onClickDeleteSkill(pos);
        btnEditFinalizar.setOnClickListener(v -> updateStudentInfo());
        btnDateLogro.setOnClickListener(v -> getDate());
        edtTxtDateLogro.setOnClickListener(v -> getDate());
        btnLogroAdd.setOnClickListener(v -> onClickAddAchievment());
        btnSkillsAdd.setOnClickListener(v -> onClickAddSkill());
        getStudent();



    }

    private void onClickDeleteAchievment(int pos){
        temporalAchievments.remove(pos);
        achivmientsAdapter.notifyDataSetChanged();
    }

    private void onClickDeleteSkill(int pos){
        temporalSkills.remove(pos);
        skillsAdapter.notifyDataSetChanged();
    }

    private void onClickAddAchievment(){
        if(     !edtTxtLogroName.getText().toString().isEmpty() &&
                !edtTxtDateLogro.getText().toString().isEmpty() &&
                !edtTxtDescriptionLogro.getText().toString().isEmpty()){
            Achievement achievement = new Achievement(
                    edtTxtLogroName.getText().toString(),
                    edtTxtDescriptionLogro.getText().toString(),
                    edtTxtDateLogro.getText().toString());
            temporalAchievments.add(achievement);
            achivmientsAdapter.notifyDataSetChanged();
            edtTxtLogroName.setText("");
            edtTxtDateLogro.setText("");
            edtTxtDescriptionLogro.setText("");
        }

    }

    private void onClickAddSkill(){
        if(!edtTxtSkills.getText().toString().isEmpty()){
            temporalSkills.add(edtTxtSkills.getText().toString());
            skillsAdapter.notifyDataSetChanged();
            edtTxtSkills.setText("");
        }

    }

    private void setRecyclerViewData(){
        layoutManagerSkills = new GridLayoutManager(this, 2);
        layoutManagerAchievments = new LinearLayoutManager(this);

        achivmientsAdapter = new AchievmentsAdapter(this,temporalAchievments,listenerAchievments);
        skillsAdapter = new SkillsAdapter(temporalSkills, this,listenerSkills);

        rcVwSkills.setHasFixedSize(true);
        rcVwSkills.setLayoutManager(layoutManagerSkills);
        rcVwSkills.setAdapter(skillsAdapter);

        rcVwLogro.setHasFixedSize(true);
        rcVwLogro.setLayoutManager(layoutManagerAchievments);
        rcVwLogro.setAdapter(achivmientsAdapter);
    }

    private void getStudent(){
        startLoading();
        String Authorization = "Bearer "+preferences.getShared().getString(Constants.USER_TOKEN,"");
        subscription = Client.getClient()
                .getStudent(TemporalData.userID,Authorization)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Student>() {
                    @Override
                    public void onCompleted() {
                        dialog.dismiss();
                        setRecyclerViewData();
                    }

                    @Override
                    public void onError(Throwable e) {
                        dialog.dismiss();
                        Toast.makeText(EditUserActivity.this, "Algo salio mal, intentalo de nuevo", Toast.LENGTH_SHORT).show();
                        finish();

                    }

                    @Override
                    public void onNext(Student student) {
                       temporalAchievments = student.getAchievements();
                       temporalSkills = student.getSkills();
                    }
                });
    }

    private void updateStudentInfo(){
        startUpdate();
        subscription = Client.getClient()
                .updateStudent(buildInfoStudent(),TemporalData.userID)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Student>() {
                    @Override
                    public void onCompleted() {
                        onCompleteUpdate();
                        dialog.dismiss();
                    }

                    @Override
                    public void onError(Throwable e) {
                        dialog.dismiss();
                        Toast.makeText(EditUserActivity.this, "Algo salio mal, intentalo de nuevo", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onNext(Student student) {
                        globalStudent = student;
                    }
                });
    }

    private void onCompleteUpdate(){
        TemporalData.temporalStudent = globalStudent;
        finish();
    }

    private Student_ buildInfoStudent(){
        List<String> favorites = new ArrayList<>();
        for(int i=0; i<TemporalData.temporalStudent.getFavorites().size(); i++){
            favorites.add(TemporalData.temporalStudent.getFavorites().get(i).getId());
        }
        Student_ student = new Student_(preferences.getShared().getString(Constants.USER_TOKEN,""),
                favorites,
                temporalSkills,temporalAchievments);
        return student;
    }

    private void getDate(){
        DatePickerDialog getDate = new DatePickerDialog(this,(view, year, month, dayOfMonth) ->{
            final int mesActual = month + 1;
            String diaFormateado =
                    (dayOfMonth < 10) ? Constants.CERO + String.valueOf(dayOfMonth) : String.valueOf(dayOfMonth);
            String mesFormateado =
                    (mesActual < 10) ? Constants.CERO + String.valueOf(mesActual) : String.valueOf(mesActual);
            edtTxtDateLogro.setText(diaFormateado + Constants.BARRA + mesFormateado + Constants.BARRA + year);
        }, anio,mes,dia);
        getDate.show();
    }

    private void startLoading(){
        dialog.setMessage("Cargando...");
        dialog.setTitle("Espere un momento");
        dialog.setIndeterminate(false);
        dialog.setCancelable(false);
        dialog.show();
    }

    private void startUpdate(){
        dialog.setMessage("Cargando...");
        dialog.setTitle("Actualizando Información");
        dialog.setIndeterminate(false);
        dialog.setCancelable(false);
        dialog.show();
    }


}
