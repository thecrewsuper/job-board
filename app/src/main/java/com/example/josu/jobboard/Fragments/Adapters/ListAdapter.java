package com.example.josu.jobboard.Fragments.Adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.josu.jobboard.R;

import java.util.List;

public class ListAdapter extends RecyclerView.Adapter<ListAdapter.ListViewHolder>{

    private List<Empresas> empresasList;
    private List<Ofertas> ofertasList;
    private boolean bMode;
    private boolean bBookmark = true;
    /**
     * bMode = true -> Aptitudes
     * bMode = false -> Logros
     */

    public ListAdapter(List<Empresas> empresasList, List<Ofertas> ofertasList, boolean bMode) {
        this.empresasList = empresasList;
        this.ofertasList = ofertasList;
        this.bMode = bMode;
    }

    public static class ListViewHolder extends RecyclerView.ViewHolder{
        //PARA bMode = TRUE
        public TextView tvNomEmpresa;
        public ImageView ivImgEmpresa;
        //PARA bMode = FALSE
        public TextView tvNomEmpresaOF, tvSalario, tvPuesto, tvLugar, tvFech;
        public ImageView ivImgEmpresaOF;
        ImageButton btnBb;


        public ListViewHolder(View itemView) {
            super(itemView);
            //EMPRESA
            tvNomEmpresa = itemView.findViewById(R.id.tvNombreEmpresa);
            ivImgEmpresa = itemView.findViewById(R.id.ivImgEmpresa);
            //OFERTAS
            tvNomEmpresaOF = itemView.findViewById(R.id.tvNombreEmpresaOF);
            tvSalario = itemView.findViewById(R.id.tvSalario);
            tvPuesto = itemView.findViewById(R.id.tvPuesto);
            tvLugar = itemView.findViewById(R.id.tvLugar);
            tvFech = itemView.findViewById(R.id.tvFecha);
            ivImgEmpresaOF = itemView.findViewById(R.id.ivImgEmpresaOF);
        }
    }

    @Override
    public ListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v;
        if (bMode){
            v = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_empresas,parent,false);
        } else {
            v = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_ofertas,parent,false);
        }
        return new ListViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ListViewHolder holder, int position) {
        if (bMode){
            holder.tvNomEmpresa.setText(empresasList.get(position).getsNombreEmpresa());
            holder.ivImgEmpresa.setImageResource(empresasList.get(position).getiImgId());
        } else {
            holder.tvNomEmpresaOF.setText(ofertasList.get(position).getsNombreEmpresaOF());
            holder.tvPuesto.setText(ofertasList.get(position).getsPuesto());
            holder.tvSalario.setText(ofertasList.get(position).getsSalario());
            holder.tvLugar.setText(ofertasList.get(position).getsLugar());
            holder.tvFech.setText(ofertasList.get(position).getsFecha());
            holder.ivImgEmpresaOF.setImageResource(ofertasList.get(position).getiImgIdOF());
            holder.btnBb.setOnClickListener(view -> {
                if (!bBookmark){
                    holder.btnBb.setImageResource(R.drawable.ic_bookmark);
                    bBookmark = true;
                } else {
                    holder.btnBb.setImageResource(R.drawable.ic_bookmark_fill);
                    bBookmark = false;
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        if (bMode){
            return empresasList.size();
        } else {
            return ofertasList.size();
        }
    }

    public static class Empresas {
        private String sNombreEmpresa;
        private int iImgId;

        public Empresas(String sNombreEmpresa, int iImgId) {
            this.sNombreEmpresa = sNombreEmpresa;
            this.iImgId = iImgId;
        }

        public String getsNombreEmpresa() {
            return sNombreEmpresa;
        }

        public int getiImgId() {
            return iImgId;
        }
    }

    public static class Ofertas {
        private String sNombreEmpresa, sSalario, sPuesto, sLugar, sFecha;
        private int iImgId;
        private ImageButton btnBookmark;

        public Ofertas(String sNombreEmpresa, String sSalario, String sPuesto, String sLugar, String sFecha, int iImgId) {
            this.sNombreEmpresa = sNombreEmpresa;
            this.sSalario = sSalario;
            this.sPuesto = sPuesto;
            this.sLugar = sLugar;
            this.sFecha = sFecha;
            this.iImgId = iImgId;
        }

        public String getsNombreEmpresaOF() {
            return sNombreEmpresa;
        }

        public int getiImgIdOF() {
            return iImgId;
        }

        public String getsSalario() {
            return sSalario;
        }

        public String getsPuesto() {
            return sPuesto;
        }

        public String getsLugar() {
            return sLugar;
        }

        public String getsFecha() {
            return sFecha;
        }
    }

}
