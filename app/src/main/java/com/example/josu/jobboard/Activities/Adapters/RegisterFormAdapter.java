package com.example.josu.jobboard.Activities.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.josu.jobboard.Fragments.Adapters.PerfilAdapter;
import com.example.josu.jobboard.POJO.Achievement;
import com.example.josu.jobboard.R;
import com.example.josu.jobboard.Utils.OnItemClickListener;

import java.util.List;

/**
 * Created by Josué on 30/05/2018.
 */

public class RegisterFormAdapter extends RecyclerView.Adapter<RegisterFormAdapter.PerfilViewHolder> {
    private List<String> aptitudesList;
    private List<Achievement> logrosList;
    private boolean bMode;
    Context context;

    public RegisterFormAdapter(List<String> aptitudesList, List<Achievement> logrosList, boolean bMode, Context context) {
        this.aptitudesList = aptitudesList;
        this.logrosList = logrosList;
        this.bMode = bMode;
        this.context = context;
    }

    /**
     * bMode = true -> Aptitudes
     * bMode = false -> Logros
     */


    @Override
    public PerfilViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        if (bMode) {
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.layout_aptitudes, parent, false);
        } else {
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.layout_logros_register, parent, false);
        }
        return new RegisterFormAdapter.PerfilViewHolder(view, bMode);
    }

    @Override
    public void onBindViewHolder(PerfilViewHolder holder, int position) {
        if (bMode) {
            holder.tvAptitud.setText(aptitudesList.get(position));
        } else {
            holder.bindData(logrosList.get(position));
        }
    }

    @Override
    public int getItemCount() {
        if (bMode) {
            return aptitudesList.size();
        } else {
            return logrosList.size();
        }
    }

    public static class PerfilViewHolder extends RecyclerView.ViewHolder {
        //PARA bMode = TRUE
        public TextView tvAptitud;
        //PARA bMode = FALSE
        public TextView tvTitulo, tvFecha;
        private OnItemClickListener listener;
        private Achievement mAchievement;

        public PerfilViewHolder(View itemView, boolean bMode) {
            super(itemView);
            if (bMode) {
                //APTUTUDES
                tvAptitud = itemView.findViewById(R.id.tvAptitud);
            } else {
                tvTitulo = itemView.findViewById(R.id.txtVwLogroTitle);
                tvFecha = itemView.findViewById(R.id.tvFecha);

            }
        }


        void bindData(Achievement achievement) {
            this.mAchievement = achievement;
            tvTitulo.setText(achievement.getTitle());
            tvFecha.setText(achievement.getDate());
        }

    }
}
