package com.example.josu.jobboard.Utils;

import com.example.josu.jobboard.Fragments.Adapters.PerfilAdapter;
import com.example.josu.jobboard.POJO.Student;
import com.example.josu.jobboard.POJO.Token;
import com.example.josu.jobboard.POJO.User;
import com.example.josu.jobboard.POJO.User_;

import java.util.ArrayList;

/**
 * Created by Josué on 01/05/2018.
 */

public class TemporalData {
    public static Token token;
    public static User user;
    public static String userID;
    public static User_ temporalUser;
    public static Student temporalStudent;
}
