package com.example.josu.jobboard.API;

import com.example.josu.jobboard.POJO.FavoriteResponse;
import com.example.josu.jobboard.POJO.Opening;
import com.example.josu.jobboard.POJO.Student;
import com.example.josu.jobboard.POJO.Student_;
import com.example.josu.jobboard.POJO.Token;
import com.example.josu.jobboard.POJO.User;
import com.example.josu.jobboard.POJO.User_;


import java.util.List;

import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;
import rx.Observable;

/**
 * Created by Josué on 05/04/2018.
 */

public interface Endpoints {
  //TODO add all the endpoints here

  @FormUrlEncoded
  @POST("/auth")
  Observable<Token> getToken(
      @Header("Authorization") String credentials,
      @Field("access_token") String access);

  @POST("/users")
  Observable<User> createUser(@Body User_ user);

  @DELETE("/users/{id}")
  Observable<Void> deleteUser(@Field("access_token") String userToken,
      @Path("id") String Userid);


  @GET("/users/me")
  Observable<User> getCurrentUser(@Query("access_token") String userToken);

  @PUT("/users/:id/password")
  Observable<User> updatePassword(@Field("password") String password);

  @PUT("/users/:id")
  Observable<User> updateUser(@Field("access_token") String userToken,
      @Field("name") String name,
      @Field("picture") String picture);

  @PUT("/users/:id")
  Observable<User> updateUser(@Field("access_token") String userToken,
      @Field("name") String name);

  @PUT("/students/{id}")
  Observable<Student> updateStudent(@Body Student_ student, @Path("id") String ProfileId);

  @PUT("/students/{id}")
  Observable<FavoriteResponse> updateStudentFavorite(@Body Student_ student, @Path("id") String ProfileId);

  @GET("/students/{id}")
  Observable<Student> getStudent(@Path("id")String profileId, @Header("Authorization")String userToken);

  @GET("/openings")
  Observable<List<Opening>> getOpenings(@Query("q")String carrer);

  @GET("/openings/{id}")
  Observable<Opening> getOpening(@Path("id")String id);


}
