package com.example.josu.jobboard.Activities.Adapters;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.josu.jobboard.POJO.Achievement;
import com.example.josu.jobboard.R;
import com.example.josu.jobboard.Utils.OnGeneralClickListener;

import java.util.List;

/**
 * Created by Josué on 29/05/2018.
 */

public class AchievmentsAdapter extends RecyclerView.Adapter<AchievmentsAdapter.ViewHolder>{
    private final Context context;
    private final LayoutInflater inflater;
    private final List<Achievement> achievements;
    private final OnGeneralClickListener listener;

    public AchievmentsAdapter(Context context, List<Achievement> achievements, OnGeneralClickListener listener) {
        this.context = context;
        this.achievements = achievements;
        this.inflater = LayoutInflater.from(context);
        this.listener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = inflater.inflate(R.layout.layout_logros_edit, parent, false);
        return new ViewHolder(itemView, listener);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Achievement achievement = achievements.get(position);
        holder.txtVwLogroDate.setText(achievement.getDate());
        holder.txtVwLogroTitle.setText(achievement.getTitle());
    }

    @Override
    public int getItemCount() {
        return achievements.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView txtVwLogroTitle, txtVwLogroDate;
        private CardView cardDeleteLogro;
        private OnGeneralClickListener listener;

        public ViewHolder(View itemView, OnGeneralClickListener listener) {
            super(itemView);
            txtVwLogroTitle = itemView.findViewById(R.id.txtVwLogroTitle);
            txtVwLogroDate = itemView.findViewById(R.id.txtVwLogroDate);
            cardDeleteLogro = itemView.findViewById(R.id.cardDeleteLogro);
            this.listener = listener;
            cardDeleteLogro.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            listener.onClick(view, getAdapterPosition());
        }
    }
}
