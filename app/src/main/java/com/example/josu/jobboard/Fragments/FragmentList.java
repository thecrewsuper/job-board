package com.example.josu.jobboard.Fragments;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.josu.jobboard.API.Client;
import com.example.josu.jobboard.Activities.MainUI;
import com.example.josu.jobboard.Activities.OpeningActivity;
import com.example.josu.jobboard.Fragments.Adapters.ListAdapter;
import com.example.josu.jobboard.Fragments.Adapters.OpeningsAdapter;
import com.example.josu.jobboard.POJO.Opening;
import com.example.josu.jobboard.R;
import com.example.josu.jobboard.Utils.Constants;
import com.example.josu.jobboard.Utils.OnGeneralClickListener;
import com.example.josu.jobboard.Utils.TemporalData;

import java.util.ArrayList;
import java.util.List;

import rx.Observer;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentList extends Fragment {
    private Context context;
    private MainUI mainUI;
    private RecyclerView rvEmpresas, rvOfertas;
    private RecyclerView.Adapter rvAdapterEmpre, rvAdapterOfert;
    private RecyclerView.LayoutManager rvLMEmpre, rvLMOfert;
    private Subscription subscription;
    private List<Opening> globalOpenings;
    private OpeningsAdapter openingsAdapter;
    private OnGeneralClickListener listener;
    List items = new ArrayList();
    List items2 = new ArrayList();

    public FragmentList() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mainUI = (MainUI) getActivity();
        context = getActivity().getApplicationContext();
        items.add(new ListAdapter.Empresas("Cementos",R.drawable.empresa));
        items.add(new ListAdapter.Empresas("Nearsoft",R.drawable.empresa));
        items.add(new ListAdapter.Empresas("Honneywell",R.drawable.empresa));
        items.add(new ListAdapter.Empresas("Farhard",R.drawable.empresa));
        items.add(new ListAdapter.Empresas("Esclavinal",R.drawable.empresa));
        items.add(new ListAdapter.Empresas("Javil",R.drawable.empresa));
        items.add(new ListAdapter.Empresas("Emerson",R.drawable.empresa));
        items.add(new ListAdapter.Empresas("Otra empresa",R.drawable.empresa));
        items.add(new ListAdapter.Empresas("Tec II",R.drawable.empresa));

        listener = (v, pos) -> onClickOpening(pos);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_fragment_list, container, false);

        //OBTENER RECYCLER
        //EMpresas
        rvEmpresas = v.findViewById(R.id.rvListEmpresa);
        rvEmpresas.setHasFixedSize(true);
        //OFERTAS
        rvOfertas = v.findViewById(R.id.rvListOfertas);

        //YSAR UN ADMIN PARA LAYOUT
        //EMPRESAS
        rvLMEmpre = new LinearLayoutManager(mainUI, LinearLayoutManager.HORIZONTAL, false);
        rvEmpresas.setLayoutManager(rvLMEmpre);
        //OFERTAS
        rvLMOfert = new LinearLayoutManager(mainUI);
        rvOfertas.setLayoutManager(rvLMOfert);

        //CREAR EL ADAPTER
        //empresas
        rvAdapterEmpre = new ListAdapter(items,null,true);
        rvEmpresas.setAdapter(rvAdapterEmpre);
        //ofertas
        //rvAdapterOfert = new ListAdapter(null, items2,false);
        //rvOfertas.setAdapter(rvAdapterOfert);
        getOpenings();
        return v;
    }

    private void getOpenings(){
        subscription = Client.getClient()
                .getOpenings(TemporalData.temporalStudent.getEducation().getDegree())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<List<Opening>>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        Toast.makeText(context, "asdasd", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onNext(List<Opening> openings) {
                        globalOpenings = openings;
                        setAdapters(openings);
                    }
                });
    }

    private void setAdapters(List<Opening> openings){
        openingsAdapter = new OpeningsAdapter(getContext(),openings, listener);
        rvOfertas.setAdapter(openingsAdapter);
        openingsAdapter.notifyDataSetChanged();
    }

    private void onClickOpening(int pos){
        Opening opening = globalOpenings.get(pos);
        Intent intent = new Intent(mainUI, OpeningActivity.class);
        intent.putExtra(Constants.OPENING_ID, opening.getId());
        startActivity(intent);
    }


}
