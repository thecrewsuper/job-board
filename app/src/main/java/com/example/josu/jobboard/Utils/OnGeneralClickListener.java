package com.example.josu.jobboard.Utils;

import android.view.View;

/**
 * Created by Josué on 28/05/2018.
 */

public interface OnGeneralClickListener {
    void onClick(View view, int position);
}
