package com.example.josu.jobboard.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.josu.jobboard.R;

public class PostRegister extends AppCompatActivity {

    Button buttonHome;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_register);
        buttonHome = findViewById(R.id.btnHome);

        buttonHome.setOnClickListener(view -> {
            Intent inHome = new Intent(this,LoginActivity.class);
            startActivity(inHome);
            finish();
        });
    }
}
