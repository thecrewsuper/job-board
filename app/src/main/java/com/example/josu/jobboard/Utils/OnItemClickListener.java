package com.example.josu.jobboard.Utils;

import android.view.View;
import com.example.josu.jobboard.POJO.Achievement;

/**
 * Created by Hugo on 25/05/18.
 */

public interface OnItemClickListener {
  void onClick(View view, int position, Achievement achievement);
}
