package com.example.josu.jobboard.Activities;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.josu.jobboard.API.Client;
import com.example.josu.jobboard.POJO.Address;
import com.example.josu.jobboard.POJO.User;
import com.example.josu.jobboard.POJO.User_;
import com.example.josu.jobboard.R;
import com.example.josu.jobboard.Utils.Constants;
import com.example.josu.jobboard.Utils.TemporalData;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import rx.Observer;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class RegisterActivity extends AppCompatActivity {
    private EditText edtTxtEmail, edtTxtPass, edtTxtCiudad,
            edtTxtSeconPass, edtTxtName, edtTxtLastName, edtTxtPhone, edtTxtEstado;
    private boolean you, shall, not, pass, vato, flag1, flag2, flag3, main;
    private Button btnContinue;
    private Pattern pattern;
    private Handler handler;
    private Thread thread;
    private User_ newUser;
    private Subscription subscription;
    private ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        edtTxtEmail = findViewById(R.id.edtTxtEmail);
        edtTxtPass = findViewById(R.id.edtTxtPass);
        edtTxtSeconPass = findViewById(R.id.edtTxtSeconPass);
        edtTxtName = findViewById(R.id.edtTxtName);
        edtTxtLastName = findViewById(R.id.edtTxtLastName);
        edtTxtCiudad = findViewById(R.id.edtTxtCiudad);
        edtTxtPhone = findViewById(R.id.edtTxtPhone);
        edtTxtEstado = findViewById(R.id.edtTxtEstado);
        btnContinue = findViewById(R.id.btnContinue);
        dialog = new ProgressDialog(this);
        putListeners();
        startVerification();
        btnContinue.setOnClickListener(v -> onClickContinue());

    }

    private void onClickContinue(){
        if(main){
            startLoading();
            buildForm();
          subscription = Client.getClient()
                  .createUser(newUser)
                  .subscribeOn(Schedulers.io())
                  .observeOn(AndroidSchedulers.mainThread())
                  .subscribe(new Observer<User>() {
                      @Override
                      public void onCompleted() {
                        dialog.dismiss();
                        TemporalData.temporalUser = newUser;
                        launchRegisterForm();
                      }

                      @Override
                      public void onError(Throwable e) {
                        dialog.dismiss();
                        Toast.makeText(RegisterActivity.this, "Error al crear usuario, intente de nuevo", Toast.LENGTH_SHORT).show();
                      }

                      @Override
                      public void onNext(User user) {

                      }
                  });
        }
    }

    private void startLoading(){
        dialog.setMessage("Espere un momento");
        dialog.setTitle("Creando Usuario");
        dialog.setIndeterminate(false);
        dialog.setCancelable(false);
        dialog.show();
    }

    private void startVerification(){
        Handler handler = getHandler();
        Runnable runnable = () ->{
            while(true){
                Message message = handler.obtainMessage();
                handler.sendMessage(message);
                checkFlags();
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
        thread = new Thread(runnable);
        thread.start();
    }

    private Handler getHandler(){
        Handler inHandler = new Handler(){
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                if(main){
                    btnContinue.setAlpha(1f);
                }else{
                    btnContinue.setAlpha(0.5f);
                }
            }
        };
        return inHandler;
    }

    private boolean checkFlags(){
        return main = you && shall && not && pass && vato && flag1 && flag2 && flag3;
    }

    private void putListeners(){
        edtTxtEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if(editable.toString().isEmpty()){
                    edtTxtEmail.setError("Debes llenar este campo");
                    you = false;
                }else{
                    pattern = Pattern.compile(Constants.EXP_REG_EMAIL);
                    Matcher matcher = pattern.matcher(editable.toString());
                    if(matcher.matches()){
                        you = true;
                    }else{
                        you = false;
                        edtTxtEmail.setError("Ingresa un correo valido");
                    }
                }
            }
        });

        edtTxtPass.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if(editable.toString().isEmpty()){
                    edtTxtPass.setError("Debes llenar este campo");
                    shall = false;
                }else if(editable.toString().length() >= 6){
                    shall = true;
                }else{
                    edtTxtPass.setError("Longitud muy pequeña");
                }
            }
        });

        edtTxtSeconPass.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if(editable.toString().isEmpty()){
                    edtTxtSeconPass.setError("Debes llenar este campo");
                    not = false;
                }else{
                    if(editable.toString().equals(edtTxtPass.getText().toString())){
                        not = true;
                    }else{
                        not = false;
                        edtTxtSeconPass.setError("Las contraseñas deben coincidir");
                    }
                }
            }
        });

        edtTxtName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if(editable.toString().isEmpty()){
                    edtTxtName.setError("Debes llenar este campo");
                    pass = false;
                }else{
                    pass = true;
                }
            }
        });

        edtTxtLastName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if(editable.toString().isEmpty()){
                    edtTxtLastName.setError("Debes llenar este campo");
                    vato = false;
                }else{
                    vato = true;
                }
            }
        });

        edtTxtCiudad.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if(editable.toString().isEmpty()){
                    edtTxtCiudad.setError("Debes llenar este campo");
                    flag1 = false;
                }else{
                    flag1 = true;
                }
            }
        });

        edtTxtPhone.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if(editable.toString().isEmpty()){
                    edtTxtPhone.setError("Debes llenar este campo");
                    flag2 = false;
                }else{
                    flag2 = true;
                }
            }
        });

        edtTxtEstado.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if(editable.toString().isEmpty()){
                    edtTxtEstado.setError("Debes llenar este campo");
                    flag3 = false;
                }else{
                    flag3 = true;
                }
            }
        });
    }

    private void launchRegisterForm(){
        startActivity(new Intent(this,RegisterFormActivity.class));
        finish();
    }

    private void buildForm(){
        newUser = new User_(
                Constants.MASTER_TOKEN,
                edtTxtEmail.getText().toString(),
                edtTxtPass.getText().toString(),
                edtTxtName.getText().toString() + " " + edtTxtLastName.getText().toString(),
                edtTxtPhone.getText().toString(),
                new Address(edtTxtCiudad.getText().toString(),
                            edtTxtEstado.getText().toString()),
                Constants.USER_KIND_STUDENT
        );
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(thread.isAlive()){
            thread.interrupt();
        }
    }
}
