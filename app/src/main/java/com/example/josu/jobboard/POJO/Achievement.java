package com.example.josu.jobboard.POJO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Josué on 20/05/2018.
 */

public class Achievement {

  @SerializedName("title")
  @Expose
  private String title;
  @SerializedName("description")
  @Expose
  private String description;
  @SerializedName("date")
  @Expose
  private String date;
  @SerializedName("_id")
  @Expose
  private String id;

  public Achievement(String title, String description, String date, String id) {
    this.title = title;
    this.description = description;
    this.date = date;
    this.id = id;
  }

  public Achievement(String title, String description, String date) {
    this.title = title;
    this.description = description;
    this.date = date;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getDate() {
    return date;
  }

  public void setDate(String date) {
    this.date = date;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  @Override
  public String toString() {
    return title + " " + date + "\n" + description;
  }
}
