package com.example.josu.jobboard.Activities.Adapters;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.josu.jobboard.R;
import com.example.josu.jobboard.Utils.OnGeneralClickListener;

import java.util.List;

/**
 * Created by Josué on 29/05/2018.
 */

public class SkillsAdapter extends RecyclerView.Adapter<SkillsAdapter.ViewHolder>{
    private final List<String> skills;
    private final Context context;
    private final LayoutInflater inflater;
    private final OnGeneralClickListener listener;

    public SkillsAdapter(List<String> skills, Context context, OnGeneralClickListener listener) {
        this.skills = skills;
        this.context = context;
        this.inflater = LayoutInflater.from(context);
        this.listener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = inflater.inflate(R.layout.layout_aptitudes_edit, parent,false);
        return new ViewHolder(itemView, listener);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        String skill = skills.get(position);
        holder.txtVwSkill.setText(skill);
    }

    @Override
    public int getItemCount() {
        return skills.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView txtVwSkill;
        private CardView cardDeleteSkill;
        private OnGeneralClickListener listener;

        public ViewHolder(View itemView, OnGeneralClickListener listener) {
            super(itemView);
            txtVwSkill = itemView.findViewById(R.id.txtVwSkill);
            cardDeleteSkill = itemView.findViewById(R.id.cardDeleteSkill);
            this.listener = listener;
            cardDeleteSkill.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            listener.onClick(view, getAdapterPosition());
        }
    }
}
