package com.example.josu.jobboard.Activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.josu.jobboard.API.Client;
import com.example.josu.jobboard.POJO.Favorite;
import com.example.josu.jobboard.POJO.FavoriteResponse;
import com.example.josu.jobboard.POJO.Opening;
import com.example.josu.jobboard.POJO.Student;
import com.example.josu.jobboard.POJO.Student_;
import com.example.josu.jobboard.POJO.User;
import com.example.josu.jobboard.R;
import com.example.josu.jobboard.Utils.Constants;
import com.example.josu.jobboard.Utils.Preferences;
import com.example.josu.jobboard.Utils.TemporalData;

import java.util.ArrayList;
import java.util.List;

import rx.Observer;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class OpeningActivity extends AppCompatActivity {

  private TextView txtVwTitle, txtVwCity, txtVwState, txtVwSalary, txtVwDegree, txtVwDescription, txtVwHide;
  private Button btnAddFavorite;
  private Intent intent;
  private String openingID;
  private Subscription subscription;
  private Opening globalOpening;
  private ProgressDialog dialog;
  private Preferences preferences;
  private int isIntent;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_opening);
    txtVwTitle = findViewById(R.id.txtVwTitle);
    txtVwState = findViewById(R.id.txtVwState);
    txtVwSalary = findViewById(R.id.txtVwSalary);
    txtVwCity = findViewById(R.id.txtVwCity);
    txtVwDegree = findViewById(R.id.txtVwDegree);
    txtVwDescription = findViewById(R.id.txtVwDescription);
    txtVwHide = findViewById(R.id.txtVwHide);
    btnAddFavorite = findViewById(R.id.btnAddFavorite);

    intent = getIntent();
    dialog = new ProgressDialog(this);
    preferences = new Preferences(this);
    openingID = intent.getStringExtra(Constants.OPENING_ID);
    isIntent = intent.getIntExtra(Constants.FAVORITE, 0);
    btnAddFavorite.setOnClickListener(v -> sendFavorite());
    startLoading();
    getOpening();
  }

  private void checkFavorites() {

    List<Opening> favorites = TemporalData.temporalStudent.getFavorites();
    for (int i = 0; i < favorites.size(); i++) {
      if (favorites.get(i).getId().equals(globalOpening.getId())) {
        btnAddFavorite.setText("Eliminar de favoritos");
        return;
      }
    }
    btnAddFavorite.setText("Añadir a favoritos");
  }

  private void startLoading() {
    dialog.setMessage("Cargando...");
    dialog.setTitle("Espere Un Momento");
    dialog.setIndeterminate(false);
    dialog.setCancelable(false);
    dialog.show();
  }

  private void startUpdate() {
    dialog.setMessage("Enviando Peticion");
    dialog.setTitle("Espere Un Momento");
    dialog.setIndeterminate(false);
    dialog.setCancelable(false);
    dialog.show();
  }

  private void getOpening() {
    subscription = Client.getClient()
        .getOpening(openingID)
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(new Observer<Opening>() {
          @Override
          public void onCompleted() {
            setData();
            checkFavorites();
          }

          @Override
          public void onError(Throwable e) {
            dialog.dismiss();
            Toast.makeText(OpeningActivity.this, "Intentelo de nuevo", Toast.LENGTH_SHORT).show();
          }

          @Override
          public void onNext(Opening opening) {
            globalOpening = opening;
          }
        });
  }

  private void sendFavorite() {
    startUpdate();
    subscription = Client.getClient()
        .updateUserFavorite(buildFavorite(), TemporalData.user.getProfile().getId())
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(new Observer<FavoriteResponse>() {
          @Override
          public void onCompleted() {
            getStdentInfo();
            Log.d("onCompleted", "onCompleted: sendFavorite");
          }

          @Override
          public void onError(Throwable e) {
            dialog.dismiss();
            Toast.makeText(OpeningActivity.this, "Algo salio mal, intentalo de nuevo",
                Toast.LENGTH_SHORT).show();
          }

          @Override
          public void onNext(FavoriteResponse favoriteResponse) {
            Log.d("onNext", "onNext: sendFavorite");
          }
        });

  }

  private void getStdentInfo() {
    String Authorization = "Bearer " + preferences.getShared().getString(Constants.USER_TOKEN, "");
    subscription = Client.getClient()
        .getStudent(TemporalData.userID, Authorization)
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(new Observer<Student>() {
          @Override
          public void onCompleted() {
            dialog.dismiss();
//            Toast.makeText(OpeningActivity.this, "Añadido correctamente!", Toast.LENGTH_SHORT)
//                .show();

          }

          @Override
          public void onError(Throwable e) {
            dialog.dismiss();
            Toast.makeText(OpeningActivity.this, "Algo salio mal, intentalo de nuevo",
                Toast.LENGTH_SHORT).show();

          }

          @Override
          public void onNext(Student student) {
            TemporalData.temporalStudent = student;
            Log.e("OnNext Get Student Info", student.getFavorites().size() + "");
            checkFavorites();
          }
        });
  }

  private Student_ buildFavorite() {
    List<String> favorites = new ArrayList<>();
//    List<Opening> favorites1 = TemporalData.temporalStudent.getFavorites();
//    for (int i = 0; i < favorites1.size(); i++) {
//      favorites.add(favorites1.get(i).getId());
//    }
    favorites.add(globalOpening.getId());
    Student_ student = new Student_(
        preferences.getShared().getString(Constants.USER_TOKEN, ""),
        favorites, TemporalData.temporalStudent.getSkills(),
        TemporalData.temporalStudent.getAchievements()
    );
    return student;
  }

  private void setData() {
    txtVwHide.setVisibility(View.INVISIBLE);
    txtVwDegree.setText("");
    txtVwTitle.setText(globalOpening.getTitle());
    txtVwCity.setText(globalOpening.getLocation().getCity());
    txtVwState.setText(globalOpening.getLocation().getState());
    txtVwSalary.setText(globalOpening.getSalary());
    txtVwDescription.setText(globalOpening.getDescription());
    for (int i = 0; i < globalOpening.getCarreer().size(); i++) {
      txtVwDegree.append(globalOpening.getCarreer().get(i) + "\n");
    }
    dialog.dismiss();
    if (isIntent == Constants.IS_FAVORITE) {
      btnAddFavorite.setVisibility(View.INVISIBLE);
    }
  }

}
