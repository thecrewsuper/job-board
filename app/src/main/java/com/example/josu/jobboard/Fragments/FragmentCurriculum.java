package com.example.josu.jobboard.Fragments;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetBehavior.BottomSheetCallback;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import android.widget.Toast;

import com.example.josu.jobboard.Activities.EditUserActivity;
import com.example.josu.jobboard.Activities.FavoriteActivity;
import com.example.josu.jobboard.Activities.MainUI;
import com.example.josu.jobboard.Activities.RegisterFormActivity;
import com.example.josu.jobboard.Fragments.Adapters.PerfilAdapter;
import com.example.josu.jobboard.POJO.Achievement;
import com.example.josu.jobboard.R;
import com.example.josu.jobboard.SettingsActivity;
import com.example.josu.jobboard.Utils.OnItemClickListener;
import com.example.josu.jobboard.Utils.TemporalData;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentCurriculum extends Fragment {

  private MainUI mainUI;
  private RecyclerView rvAptitud, rvLogros;
  private RecyclerView.Adapter rvAdapterApti, rvAdapterLogr;
  private RecyclerView.LayoutManager rvLMApti, rvLMLogr;
  private TextView tvNom, tvCorreo, tvCarrera, tvGen, tvTel, tvCd;
  private Button tvOfert;
  private ImageButton imgBtnEdit, imgBtnSettings;
  private CircleImageView ivPP;
  private Context context;
  private OnItemClickListener listener;

  Button btnBottomSheet;
  LinearLayout layoutBottomSheet;
  BottomSheetBehavior sheetBehavior;
  TextView tvAchievementTitle, tvAchievementDate, tvAchievementDescription;


  public FragmentCurriculum() {
    // Required empty public constructor
  }

  @Override
  public void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    context = getActivity();
    mainUI = (MainUI) getActivity();

  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    // Inflate the layout for this fragment
    View v = inflater.inflate(R.layout.fragment_fragment_curriculum, container, false);

    layoutBottomSheet = v.findViewById(R.id.bottom_sheet);
    btnBottomSheet = v.findViewById(R.id.btnCloseBS);
    tvAchievementTitle = v.findViewById(R.id.etTituloBS);
    tvAchievementDate = v.findViewById(R.id.etFechaBS);
    tvAchievementDescription = v.findViewById(R.id.etDescripBS);
    imgBtnEdit = v.findViewById(R.id.imgBtnEdit);
    imgBtnSettings = v.findViewById(R.id.imgBtnSettings);
    tvOfert = v.findViewById(R.id.tvOfert);

    sheetBehavior = BottomSheetBehavior.from(layoutBottomSheet);
    /**
     * bottom sheet state change listener
     * we are changing button text when sheet changed state
     * */
    sheetBehavior.setBottomSheetCallback(new BottomSheetCallback() {
      @Override
      public void onStateChanged(@NonNull View bottomSheet, int newState) {
        switch (newState) {
          case BottomSheetBehavior.STATE_HIDDEN:
            break;
          case BottomSheetBehavior.STATE_EXPANDED: {
//            btnBottomSheet.setText("Close Sheet");
          }
          break;
          case BottomSheetBehavior.STATE_COLLAPSED: {
//            btnBottomSheet.setText("Expand Sheet");
          }
          break;
          case BottomSheetBehavior.STATE_DRAGGING:
            break;
          case BottomSheetBehavior.STATE_SETTLING:
            break;
        }
      }

      @Override
      public void onSlide(@NonNull View bottomSheet, float slideOffset) {
      }
    });

    btnBottomSheet
        .setOnClickListener(view -> sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED));
    listener = (view, position, achievement) -> {
      toggleBottomSheet(achievement);
    };

    //TextViews
    tvNom = v.findViewById(R.id.tvNom);
    tvCorreo = v.findViewById(R.id.tvCorreo);
    tvCarrera = v.findViewById(R.id.tvCarrera);
    tvGen = v.findViewById(R.id.tvGen);
    tvTel = v.findViewById(R.id.tvTel);
    tvCd = v.findViewById(R.id.tvCd);

    //OBTENER RECYCLER
    //APTITUDES
    rvAptitud = v.findViewById(R.id.rvApti);
    rvAptitud.setHasFixedSize(true);
    //LOGROS
    rvLogros = v.findViewById(R.id.rvLogros);
    rvLogros.setHasFixedSize(true);
    //CircleImageView
    ivPP = v.findViewById(R.id.ivPP);

    //YSAR UN ADMIN PARA LAYOUT
    //APTITUDES
    rvLMApti = new GridLayoutManager(mainUI, 4);
    rvAptitud.setLayoutManager(rvLMApti);
    //LOGROS
    rvLMLogr = new LinearLayoutManager(mainUI);
    rvLogros.setLayoutManager(rvLMLogr);

    //CREAR EL ADAPTER
    //APTITUDES
    rvAdapterApti = new PerfilAdapter(TemporalData.temporalStudent.getSkills(), null, true,
        context, listener);
    rvAptitud.setAdapter(rvAdapterApti);
    //LOGROS
    rvAdapterLogr = new PerfilAdapter(null, TemporalData.temporalStudent.getAchievements(), false,
        context, listener);
    rvLogros.setAdapter(rvAdapterLogr);

    imgBtnEdit.setOnClickListener(view -> startEditInfoUser());
    tvOfert.setOnClickListener(view -> onClickFavoritas());
    imgBtnSettings.setOnClickListener(view -> onclickSettings());

    setData();

    return v;
  }


  //Este metodo coloca la informacion referente del usuario que se localiza en la clase TemporalData
  //Los valores de TemporalData por lo general son obtenidos y asignados del API
  private void setData() {
    tvNom.setText(TemporalData.user.getName());
    tvCorreo.setText(TemporalData.user.getEmail());
    tvTel.setText(TemporalData.user.getPhone());
    tvCarrera.setText(TemporalData.temporalStudent.getEducation().getDegree());
    tvGen.setText(TemporalData.temporalStudent.getEducation().getDate());
    tvCd.setText(TemporalData.user.getAddress().getState() + ", " + TemporalData.user.getAddress()
        .getCity());
    //Usamos la libreria de piccaso para cargar una url de una imagen dentro del imageView
    Picasso.with(context)
        .load(TemporalData.user.getPicture())
        .placeholder(R.drawable.user_image)
        .noFade()
        .into(ivPP);
  }

  private void onclickSettings(){
    startActivity(new Intent(context, SettingsActivity.class));
    mainUI.finish();
  }

  private void onClickFavoritas(){
    startActivity(new Intent(context, FavoriteActivity.class));
  }

  private void startEditInfoUser(){
    startActivity(new Intent(context, EditUserActivity.class));
  }

  public void toggleBottomSheet(Achievement achievement) {


    if (sheetBehavior.getState() != BottomSheetBehavior.STATE_EXPANDED) {
      sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
      tvAchievementTitle.setText(achievement.getTitle());
      tvAchievementDate.setText(achievement.getDate());
      tvAchievementDescription.setText(achievement.getDescription());
    } else {
      sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
    }
  }

  @Override
  public void setUserVisibleHint(boolean isVisibleToUser) {
    super.setUserVisibleHint(isVisibleToUser);
    if(isVisibleToUser){
      if(rvAdapterLogr != null){
        rvAdapterLogr.notifyDataSetChanged();
      }
      if(rvAdapterApti != null){
        rvAdapterApti.notifyDataSetChanged();
      }
    }
  }
}
