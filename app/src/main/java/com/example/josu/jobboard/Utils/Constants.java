package com.example.josu.jobboard.Utils;

/**
 * Created by Josué on 04/04/2018.
 */

public class Constants {
    //Agregar todas las constantes en está clase
    public static final int IS_FAVORITE = 1000;
    public static final String FAVORITE = "IS_FAVORITE";
    public static final String BASE_URL = "https://job-board-itchii.herokuapp.com";
    public static final String MASTER_TOKEN = "VwVvzFaFpig7ddaWAcnErtEgHNwYscgv";
    public static final String APP_NAME = "JOB-BOARD";
    public static final String USER_TOKEN = "USER_TOKEN";
    public static final String USER_NAME = "USER_NAME";
    public static final String USER_EMAIL = "USER_EMAIL";
    public static final String USER_ID = "USER_ID";
    public static final String LOGIN_FLAG ="LOGIN_FALG";
    public static final String OPENING_ID ="OPENING_ID";
    public static final String USER_KIND_STUDENT = "Student";
    public static final String STEP_POSITION = "STEP_POSITION";
    public static final String CREDENTIALS = "CREDENTIALS";
    public static final String CERO = "0";
    public static final String BARRA = "/";
    public static final String EXP_REG_EMAIL = "^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
            +"((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
            +"[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
            +"([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
            +"[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
            +"([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$";
    public static final String[] DEGREE = {"Ingeniería en Informática", "Ingeniería en Diseño Industrial",
            "Ingeniería en Gestión Empresarial", "Ingeniería en Industrial",
            "Ingeniería en Sistemas Computacionales", "Arquitectura", "Licenciatura en administración"};
}
